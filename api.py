#!/usr/bin/python3
import flask
from flask import request, jsonify
import datetime
import threading
import time

BASE_URL="/api/"
VERSION="1"
BASE_PATH=BASE_URL+"v"+VERSION


DEFAULT_TIME=3*60

print("STARTING SERVER")

toast_inside=False
toast_end_time=datetime.datetime.now()
current_toast_time=0


app = flask.Flask(__name__)
#app.config["DEBUG"] = True

def delay_eject(timeout):
    time.sleep(timeout)
    print("TIMEOUT OVER. EJECTING.")
    toast_eject()

@app.route(BASE_PATH+'/', methods=['GET'])
def home():
    return "<h1>Distant Reading Archive</h1><p>This site is a prototype API for distant reading of science fiction novels.</p>"

@app.route(BASE_PATH+'/toast/make', methods=['GET'])
def toast_make():
    global toast_inside
    global toast_end_time
    global current_toast_time
    if 'time' in request.args:
        print("TIME-ARGUMENT: "+str(request.args['time']))
        timeout=int(request.args['time'])
    else:
        timeout=DEFAULT_TIME
    current_toast_time=timeout
    toast_inside=True
    toast_end_time=datetime.datetime.now()+datetime.timedelta(seconds=current_toast_time)
    print(str(datetime.timedelta(seconds=current_toast_time)))
    print("STARTING MOTOR MOVE")
    print("STARTING TOAST PROCESS USING TIME "+str(timeout))
    print("STARTING THREAD")
    x = threading.Thread(target = delay_eject, args=[timeout])
    x.start()
    return jsonify(True)

@app.route(BASE_PATH+'/toast/eject', methods=['GET'])
def toast_eject():
    global toast_inside
    global toast_end_time
    global current_toast_time
    print("EJECTING TOAST")
    toast_inside=False
    current_toast_time=0
    toast_end_time=datetime.datetime.now()
    try:
        return jsonify(True)
    except:
        print("Running in thread. Skipping return.")
    

@app.route(BASE_PATH+'/toast/state', methods=['GET'])
def toast_state():
    global toast_inside
    global toast_end_time
    global current_toast_time
    remainingTime=(toast_end_time-datetime.datetime.now()).seconds
    if remainingTime < 0:
        remainingTime=0
    
    state = {
        'inside': toast_inside,
        'remainingTime': remainingTime,
        'totalTime': current_toast_time
    }
    print("CHECKING TOAST")
    return jsonify(state)
app.run()