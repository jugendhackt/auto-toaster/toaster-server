# AutoToaster Server API ocumentation

Please call by:
`http://*pi-ip*/api/v1/toast/*function*`
## List of functions
|Function|Parameter|Response|
|----------------|-------------------------------|-----------------------------|
|make|`[int time]` Toasting time in seconds | `bool` Was the action successful? |
|state| *None* | `array { bool inside [, int remainingTime, int totalTime]}` Is a toast inside? The remaining toasting time. The total toast time|
|eject| *None* | `bool` Was the action successful? |